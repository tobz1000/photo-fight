# Photo Fight!

__[Demo](https://reverent-chandrasekhar-aa21f0.netlify.app/)__

To run locally (requires nodejs & yarn):

- [Disable CORS in your browser (temporarily!)](https://dev.to/andypotts/avoiding-cors-errors-on-localhost-in-2020-4mfn)
    - The clickasnap API doesn't send the required CORS header for usage outside of clickasnap itself, so this is a necessary workaround when running locally (without setting up a local server).
- Run `yarn dev` from root repo dir
- Navigate to `http://localhost:3000`