import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import ErrorBoundary from "./ErrorBoundary";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { ApolloProvider } from "@apollo/client/react";

function getApiUrl(): string {
    const apiBaseUrl = "https://api.clickasnap.com/graphql";

    if (window.location.host.endsWith("netlify.app")) {
        // We can use the configured universal proxy

        return `/cors-proxy/${apiBaseUrl}`;
    } else {
        console.warn(
            "Photo API does not return correct CORS header. Disable CORS in your browser: https://dev.to/andypotts/avoiding-cors-errors-on-localhost-in-2020-4mfn"
        );

        return apiBaseUrl;
    }
}

const client = new ApolloClient({
    uri: getApiUrl(),
    cache: new InMemoryCache(),
});

ReactDOM.render(
    <React.StrictMode>
        <ErrorBoundary>
            <ApolloProvider client={client}>
                <App />
            </ApolloProvider>
        </ErrorBoundary>
    </React.StrictMode>,
    document.getElementById("root")
);
