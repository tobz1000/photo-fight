import React, { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faDiceOne,
    faDiceTwo,
    faDiceThree,
    faDiceFour,
    faDiceFive,
    faDiceSix,
} from "@fortawesome/free-solid-svg-icons";

const TARGET_IMG_HEIGHT_PX = 1000;

const IMAGE_QUERY = gql`
    query($keyword: String!) {
        imageSearch(text: $keyword) {
            results {
                file
                title
            }
        }
    }
`;

type ImageQueryInput = { keyword: string };

type ImageSearchResult = {
    imageSearch: {
        results: ImageSearchEntry[];
    };
};

type ImageSearchEntry = {
    file: string;
    title: string;
};

function ActionButton({
    children,
    onClick,
    disabled,
}: {
    children: React.ReactNode;
    onClick: () => void;
    disabled: boolean;
}) {
    return (
        <button
            disabled={disabled}
            onClick={onClick}
            style={{
                borderColor: "darkgray",
                fontFamily: "'Press Start 2P', cursive",
                fontSize: "2vh",
                height: "100%",
                width: "100%",
            }}
        >
            {children}
        </button>
    );
}

function App() {
    const [searchInput, setSearchInput] = useState("");
    const [currentKeyword, setCurrentKeyword] = useState<string | null>(null);

    const effectiveInput = searchInput.trim();
    const inputEntered = effectiveInput !== "";
    const goButtonActive = inputEntered && effectiveInput !== currentKeyword;

    function enterKeyword() {
        setCurrentKeyword(effectiveInput);
        setSearchInput(effectiveInput);
    }

    return (
        <>
            <div style={{ gridArea: "se", display: "flex" }}>
                <input
                    style={{
                        padding: 0,
                        borderWidth: 0,
                        width: "100%",
                        height: "100%",
                        fontFamily: "'Coda Caption', sans-serif",
                        WebkitTextFillColor: "peru",
                        backgroundColor: "beige",
                        fontSize: "2.5vh",
                        flexGrow: 1,
                    }}
                    placeholder="Enter keyword..."
                    onChange={(e) => setSearchInput(e.target.value)}
                    onKeyPress={(e) => {
                        if (e.code === "Enter") {
                            enterKeyword();
                        }
                    }}
                    value={searchInput}
                />
                <div style={{ flexGrow: 0 }}>
                    <ActionButton disabled={!goButtonActive} onClick={enterKeyword}>
                        Go
                    </ActionButton>
                </div>
            </div>

            {currentKeyword ? <Arena keyword={currentKeyword} /> : <></>}
        </>
    );
}

type ArenaState =
    | {
          kind: "start";
      }
    | {
          kind: "rolling";
      }
    | { kind: "finished"; dice: AllDiceState };

type AllDiceState = { a1: number; a2: number; b1: number; b2: number };

function randomDieRoll(): number {
    return Math.floor(Math.random() * 6) + 1;
}

function Arena({ keyword }: { keyword: string }): JSX.Element {
    const apiResult = useQuery<ImageSearchResult, ImageQueryInput>(IMAGE_QUERY, {
        variables: { keyword },
    });

    // TODO: traverse search query pagination instead of repeating first page
    const [imgAIndex, setImgAIndex] = useState(0);
    const [imgBIndex, setImgBIndex] = useState(1);
    const [nextImgIndex, setNextImgIndex] = useState(2);
    const [arenaState, setArenaState] = useState<ArenaState>({
        kind: "start",
    });

    useEffect(() => {
        if (arenaState.kind === "rolling") {
            setTimeout(() => {
                setArenaState({
                    kind: "finished",
                    dice: { a1: randomDieRoll(), a2: randomDieRoll(), b1: randomDieRoll(), b2: randomDieRoll() },
                });
            }, 2000);
        }
    }, [arenaState.kind]);

    if (apiResult.error) {
        throw apiResult.error;
    }

    const wordArtStyle: React.CSSProperties = {
        fontFamily: "'Permanent Marker', cursive",
        fontSize: "5vh",
        background: "linear-gradient(orange, yellow)",
        backgroundClip: "text",
        WebkitTextFillColor: "transparent",
        WebkitTextStrokeWidth: 1,
        WebkitTextStrokeColor: "black",
        textAlign: "center",
    };
    const FightButton = ({ text, disabled }: { text: string; disabled: boolean }) => (
        <ActionButton onClick={() => setArenaState({ kind: "rolling" })} disabled={disabled}>
            {text}
        </ActionButton>
    );
    const NewChallengerButton = ({ toSwitchOut }: { toSwitchOut: "a" | "b" }) => (
        <ActionButton
            onClick={() => {
                if (toSwitchOut === "a") {
                    setImgAIndex(nextImgIndex);
                } else {
                    setImgBIndex(nextImgIndex);
                }

                setNextImgIndex(nextImgIndex + 1);
                setArenaState({ kind: "start" });
            }}
            disabled={false}
        >
            New Challenger!
        </ActionButton>
    );

    const GameMessage = ({ children }: { children: React.ReactNode }) => <h1 style={wordArtStyle}>{children}</h1>;

    const title = <h1 style={wordArtStyle}>Photo Fight!</h1>;

    let gameMessage: JSX.Element;
    let actionButton: JSX.Element;

    let fighterALost = false;
    let fighterBLost = false;

    switch (arenaState.kind) {
        case "start":
            gameMessage = (
                <GameMessage>
                    Get
                    <br />
                    ready!
                </GameMessage>
            );
            actionButton = <FightButton text="Fight!" disabled={false} />;
            break;

        case "rolling":
            gameMessage = <></>;
            actionButton = <FightButton text="Fight!" disabled={true} />;
            break;

        case "finished":
            const scoreA = arenaState.dice.a1 + arenaState.dice.a2;
            const scoreB = arenaState.dice.b1 + arenaState.dice.b2;

            if (scoreA > scoreB) {
                fighterBLost = true;
                gameMessage = (
                    <GameMessage>
                        Photo A<br />
                        wins!
                    </GameMessage>
                );
                actionButton = <NewChallengerButton toSwitchOut="b" />;
            } else if (scoreB > scoreA) {
                fighterALost = true;
                gameMessage = (
                    <GameMessage>
                        Photo B<br />
                        wins!
                    </GameMessage>
                );
                actionButton = <NewChallengerButton toSwitchOut="a" />;
            } else {
                gameMessage = <GameMessage>Draw!</GameMessage>;
                actionButton = <FightButton text="Rematch!" disabled={false} />;
            }
            break;
    }

    let fighterA: JSX.Element;
    let fighterB: JSX.Element;

    if (apiResult.loading) {
        fighterA = <FighterPlaceholder />;
        fighterB = <FighterPlaceholder />;
    } else {
        const imageEntries = apiResult.data!.imageSearch.results;
        const imgA = imageEntries[imgAIndex % imageEntries.length];
        const imgB = imageEntries[imgBIndex % imageEntries.length];

        fighterA = <Fighter file={imgA.file} title={imgA.title} lost={fighterALost} />;
        fighterB = <Fighter file={imgB.file} title={imgB.title} lost={fighterBLost} />;
    }

    const dieElement = (d: keyof AllDiceState) => {
        let dieProps: DieProps;

        switch (arenaState.kind) {
            case "start":
                // Start game with all sixes up
                dieProps = { kind: "rolled", value: 6 };
                break;
            case "rolling":
                dieProps = { kind: "rolling" };
                break;
            case "finished":
                dieProps = { kind: "rolled", value: arenaState.dice[d] };
                break;
        }

        return <Die {...dieProps} />;
    };

    return (
        <>
            <div style={{ gridArea: "ti", placeSelf: "center" }}>{title}</div>
            <div style={{ gridArea: "fa" }}>{fighterA}</div>
            <div style={{ gridArea: "da1", placeSelf: "center" }}>{dieElement("a1")}</div>
            <div style={{ gridArea: "da2", placeSelf: "center" }}>{dieElement("a2")}</div>
            <div style={{ gridArea: "gm", placeSelf: "center" }}>{gameMessage}</div>
            <div style={{ gridArea: "db1", placeSelf: "center" }}>{dieElement("b1")}</div>
            <div style={{ gridArea: "db2", placeSelf: "center" }}>{dieElement("b2")}</div>
            <div style={{ gridArea: "fb" }}>{fighterB}</div>
            <div style={{ gridArea: "ab", placeSelf: "center", width: "100%", height: "6vh" }}>{actionButton}</div>
        </>
    );
}

function FighterPlaceholder(): JSX.Element {
    return <FighterInner title="Awaiting contestant..." lost={false} />;
}

function Fighter({ file, title, lost }: { file: string; title: string; lost: boolean }): JSX.Element {
    const imageUrl = `https://cdn.clickasnap.com/0x${TARGET_IMG_HEIGHT_PX}/${file}`;
    const [imageLoaded, setImageLoaded] = useState(false); // TODO: this doesn't reset to false on change of props...?

    // Load image into hidden <img /> for the event callback once it's loaded. Actually display as
    // a background for the zoom/crop styling.

    return (
        <>
            <img style={{ display: "none" }} src={imageUrl} onLoad={() => setImageLoaded(true)} />

            {imageLoaded ? (
                <FighterInner
                    style={{
                        backgroundImage: `url(${imageUrl})`,
                    }}
                    title={title}
                    lost={lost}
                />
            ) : (
                <FighterPlaceholder />
            )}
        </>
    );
}

function FighterInner({ style, title, lost }: { style?: React.CSSProperties; title: string; lost: boolean }) {
    return (
        <div
            style={{
                backgroundSize: "cover",
                backgroundPosition: "center",
                width: "100%",
                height: "100%",
                fontFamily: "'Coda Caption', sans-serif",
                fontSize: "2.5vh",
                WebkitTextFillColor: "beige",
                WebkitTextStrokeWidth: 0.4,
                WebkitTextStrokeColor: "black",
                ...style,
            }}
        >
            <span style={{ position: "absolute", zIndex: 1 }}>{title}</span>
            <div
                style={{
                    animation: lost ? "0.5s fighterlost" : undefined,
                    height: "100%",
                    width: "100%",
                    backgroundColor: lost ? "red" : undefined,
                    opacity: 0.4,
                }}
            />
        </div>
    );
}

type DieProps = { kind: "rolling" } | { kind: "rolled"; value: number };

function Die(props: DieProps): JSX.Element {
    const dieIconDefinition = (i: number) => {
        switch (i) {
            case 1:
                return faDiceOne;
            case 2:
                return faDiceTwo;
            case 3:
                return faDiceThree;
            case 4:
                return faDiceFour;
            case 5:
                return faDiceFive;
            case 6:
                return faDiceSix;
            default:
                throw `Unexpected die value: ${i}`;
        }
    };

    const [internalValue, setInternalValue] = useState(1);
    const [intervalId, setIntervalId] = useState<number | null>(null);

    useEffect(() => {
        if (props.kind === "rolling") {
            const interval = setInterval(() => setInternalValue(randomDieRoll()), 400);
            setIntervalId(interval);
        } else {
            setInternalValue(props.value);

            if (intervalId !== null) {
                clearInterval(intervalId);
                setIntervalId(null);
            }
        }
    }, [props.kind]);

    const iconDefinition = dieIconDefinition(internalValue);

    // Key causes animation re-trigger on each change of value
    return (
        <div key={internalValue} className="fa-container" style={{ animation: "0.4s rot90" }}>
            <FontAwesomeIcon icon={iconDefinition} className="die-svg" />
        </div>
    );
}

export default App;
